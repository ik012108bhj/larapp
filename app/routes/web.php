<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::fallback(function () {
//Route::get('/', function () {
    return view('welcome', [ 
        'api' => env('PUBLIC_API','http://localhost:8888/api/v1/users'),
        'app' => env('APP_NAME','Docker LaraWeb')
    ]);
});
