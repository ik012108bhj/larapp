<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>LARAWEB</title>
  <style> 
    body { 
      background: #FEF9E7; 
    }
    * {
      font-family: 'Trebuchet MS', sans-serif;
      font-size: 100%;
    }
    ul { 
      padding: 0; 
      list-style-type: none;
    }
    button{
      font-size: 70%;
    }
    .row {
      display: grid;
      grid-template-columns: 15rem 1fr;
    }
    .left{
      display: grid;
      grid-template-columns: 1fr 4fr 1fr;
      }
  </style>
  </head>
  <body>
    &emsp;&emsp;<a href="/" title="APP}">{{ $app }}</a></br>
    &emsp;&emsp;<a href="{{ $api }}" title="API}">{{ $api }}</a>
    <div class="row">
      <div class="column left">
        <div></div>
        <div>
        <h4>Form User</h4>
        <input type="hidden" id="_id" name="_id">
        <input id="name" type="text" placeholder="name" name="name"></br>
        <input id="email" type="text" placeholder="email" name="email"></br>
        </br>
        <button id="bNew" onclick="onNew()" title="INSERT - API POST">ADD</button>
        <button disabled id="bUpdate" onclick="onUpdate()" title="UPDATE - API PUT">UPDATE</button>
        </div>
        <div></div>
      </div>
      <div class="column right">
        <h4>Lista User</h4>
        <ul id="users"></ul>
      </div>
    </div>
    <script>
    const apiusers = '{{ $api }}';

    // MANEJO DEL DOM
    var setUsersList = (ul, rows) => {
      ul.innerHTML = "";
      rows.forEach(user => {
          var li = document.createElement("li");
          li.innerHTML = '\
          <button onclick="api_delete_id('+user._id+')" title="DELETE - API DELETE">DEL</button>\
          <button onclick="setUserForm('+user._id+',`'+user.name+'`,`'+user.email+'`)" title="EDIT">EDIT</button>\
          &bull; <a href="' + apiusers + '/'+user._id+'">'+user.name+', '+user.email+'</a>';
          ul.appendChild(li);
      });
    }

    var setUserForm = ( id , name, email ) =>{
      document.getElementById('_id').value=id
      document.getElementById('name').value=name
      document.getElementById('email').value=email
      document.getElementById('bUpdate').disabled=false
      document.getElementById('bNew').disabled=true
    }

    function onNew() { 
      api_post(
        document.getElementById('name').value, 
        document.getElementById('email').value
      )
    }

    function onUpdate () { 
      api_update_id(
        document.getElementById('_id').value, 
        document.getElementById('name').value, 
        document.getElementById('email').value
      )
      document.getElementById('bUpdate').disabled=true
      document.getElementById('bNew').disabled=false
    }

    window.onload = () => {
      api_get_all();
    }

    // API
    // INSERT
    var api_post =  (name, email) => {
    console.log('post: ')
    var body = JSON.stringify({ name: name, email: email })
    console.log(body)

    fetch( apiusers, {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: body
    }).then( response => {
        location.replace(location.pathname)
        return response
    }).then( response => console.log(response))
    }
    // UPDATE
    var api_update_id = (id, name, email) => {
      console.log('put: ')
      var body = JSON.stringify({  _id: id, name: name, email: email })
      console.log(body)

      fetch( apiusers, {
        method: 'put',
        headers: { 'Content-Type': 'application/json' },
        body: body
      }).then(response => {
        if (response.ok) return response.json()
      }).then(data => {
        ////window.location.reload()
        location.replace(location.pathname)
        console.log(data)
      })
    }
    // DELETE
    var api_delete_id =  (id) => {
      console.log('delete: '+id)
      fetch( apiusers + '/'+id, {
        method: 'delete',
        headers: { 'Content-Type': 'application/json' }
      }).then(response=> {
        console.log(response)
        //window.location.reload()
        location.replace(location.pathname)
      })
    }
    // GET ALL
    var api_get_all =  () => {
      console.log('get')
      fetch( apiusers, {
        method: 'get',
        headers: { 'Content-Type': 'application/json' }
      }).then(response => {
        console.log(response)
        if (response.ok) return response.json()
      }).then(data => {
        setUsersList(document.getElementById('users'), data)
        console.log(data)
      })
    }
        

  </script>
  </body>
</html>
